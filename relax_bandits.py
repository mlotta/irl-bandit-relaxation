import numpy as np
import numpy.random as rnd
from scipy.optimize import linprog

from joblib import Memory
location = './cachedir'
memory = Memory(location, verbose=0)

def P_construct(nb_state):
    """
    Generates a random transition matrix with nb_state states
    """
    P = np.zeros((nb_state, nb_state))
    for row_nb in range(nb_state):
        Y = rnd.exponential(size=nb_state)
        P[row_nb, :] = Y/sum(Y)
    return P

def R_construct(nb_state):
    """
    Generates a random reward vector with nb_state states
    """
    return rnd.uniform(0, 1, nb_state)

def PR_construct(nb_state, seed = None):
    """
    Generates a set {P_0, P_1, R_0, R_1}  with nb_state states
    nb_state: size of the matrices and vectors
    seed: generation seed 
    """
    if seed is not None: rnd.seed(seed)

    return P_construct(nb_state), P_construct(nb_state),  R_construct(nb_state),  R_construct(nb_state)

def transition(P, state):
    """
    Simulates the transition of a bandit to another state according
    to the transition matrix P
    """
    return np.argmax(rnd.multinomial(1, P[state, :]))

def bandit_to_rho(bandits, S):
    """
    Converts a vector of size N of the states of different bandits
    to a vector of size |S| of proportions of bandits in state i at 
    arg i.
    bandits: size N vector of states of bandits with value in S
    S: state space
    """
    return np.array([sum(bandits == s) for s in S])/bandits.shape[0]

def rho_index(ind_a, ind_s, ind_t, S, T_max):
    """
    Converts index of a three dimensionnal array to single dimensionnal
    ind_a: action 0 or 1
    ind_s: index of state s in S
    ind_t: time t
    S: state space
    T_max: time horizon
    """
    return ind_a*S.shape[0]*T_max + ind_s*T_max + ind_t 

def lp_solver(A, S, T_max, rho, P, R, alpha):
    """
    Creates the condition matrices and vectors to eventually solve
    the linear programm associated with the parameters:
    A: action A, typically [0, 1]
    S: state space
    T_max: time horizon
    rho: initial condition
    P = [P0, P1]; transition matrices
    R = [R0, R1]: reward vectors
    alpha: proportion of bandits to activate at each timestep
    """
    T = np.array(range(1, T_max+1))

    total_len = A.shape[0] * S.shape[0] * T_max

    # rewards
    reward = np.array([0. for _ in range(total_len)])
    cpt = 0
    for a in A:
        for s in S:
            for t in T:
                reward[cpt] = -R[a][s]   #-r(s, a)
                cpt += 1

    # budget condition
    budget = np.ones(T_max) * alpha

    mat_budget = np.zeros((T_max, total_len))
    for t in T:
        for i, s in enumerate(S):
            mat_budget[t-1, rho_index(1, i, t-1, S, T_max)] = 1


    # initial occupation
    init_occupation = rho.copy()
    
    mat_init_occupation = np.zeros((S.shape[0], total_len))
    for i, s in enumerate(S):
        for j, a in enumerate(A):
            mat_init_occupation[i, rho_index(j, i, 0, S, T_max)] = 1

    #flow balance
    flow_balance = np.zeros((S.shape[0]*(T_max-1)))
    mat_flow_balance = np.zeros((S.shape[0]*(T_max-1),total_len))

    for i, s in enumerate(S):
        for t in T[1:]:
            for j, a in enumerate(A):
                mat_flow_balance[t-2 + i*(T_max-1), rho_index(j, i, t-1, S, T_max)] = 1
                for k, s2 in enumerate(S):
                    mat_flow_balance[t-2 + i*(T_max-1), rho_index(j, k, t-2, S, T_max)] = -P[a][s2, s]
                    
            
    # concatenating the conditions
    Aeq = np.concatenate((mat_budget, mat_init_occupation, mat_flow_balance), axis = 0)
    beq = np.concatenate((budget, init_occupation, flow_balance))
    
    rho_star = linprog(reward, A_eq = Aeq, b_eq = beq, bounds=(0, None))
    
    return rho_star.x, rho_star.fun
lp_solver_joblib = memory.cache(lp_solver)


def determine_action(t, policy, bandits, N, alpha):
    """
    From a policy and a bandit vector, chooses which bandits
    to activate
    t: timestep for the policy
    bandits: bandit vector
    N: number of bandits
    alpha: proportion of bandits to activate, typically alpha*N is
    the number of activated bandits

    return : a vector of actions in A
    """
    action_t = np.zeros(N)
    left_to_activate = int(round(alpha*N))


    for state in np.argsort(-policy[t]):             
        for i, bandit in enumerate(bandits):
            if left_to_activate <= 0:
                break
            if bandit == state:
                action_t[i] = 1
                left_to_activate -= 1

    return action_t



def print_debug(t, policy, bandits, action_t):
    """
    debugging function
    t: timestep for the policy
    bandits: bandit vector
    action_t : a vector of actions in A
    """
    print("t :", t)
    print("Policy recommends : ", np.argsort(policy))
    print("Bandits are in states : ", bandits)
    print("Bandits number ", action_t, "are selected")
    print("="*50)
    
    
def experiment(nb_state, alpha, N, T_max, P, R, debug=False, step_resolution=1, bandits=None, seed=1, joblib_id=0):
    """
    Plays T_max timesteps from the initial condition bandits according
    to the relaxation policy
    nb_state: number of state of the problem
    alpha: proportion of bandits to activate at each timestep
    N: number of bandits
    T_max: time_horizon
    P=(P0, P1): transition matrices
    R=(R0,R1): reward vectors
    debug: activate a debug function for printing useful informations
    step_resolution: number of timesteps before solving the LP again
    seed: a seed for transition of the bandits
    joblib_id: the function caches the calls to the LP solver when set to 0
    """
    A = np.array([0, 1])
    P0, P1 = P
    R0, R1 = R
    S = np.array(range(nb_state))
    policy_1 = np.zeros((T_max, S.shape[0]))
    values = np.zeros(T_max)
    
    
    if bandits is None:
        bandits = np.zeros(N, dtype = int)
    
    t = 0
    while t < T_max:
        rho = bandit_to_rho(bandits, S)
        
        T_left = T_max - t
        
        if joblib_id == 0:
            solv, value_bound = lp_solver_joblib(A, S, T_left, rho, [P0, P1], [R0, R1], alpha)
        else:
            solv, value_bound = lp_solver(A, S, T_left, rho, [P0, P1], [R0, R1], alpha)

        if T_left == T_max: bound = -value_bound

        for step in range(min(step_resolution, T_left)):
            # policy - probability to take action a = 1
            policy_1[t] = np.array([solv[1 * S.shape[0]*T_left + s * T_left + step] 
                                        / sum([solv[a2 * S.shape[0]*T_left + s * T_left + step] for a2 in A]) 
                                      for s in S])    

            # arms to activate
            action_t = determine_action(t, policy_1, bandits, N, alpha)

            #debugging 
            if debug:
                print_debug(t, policy_1[t], bandits, action_t)

            # action is taken, an observation is made
            values[t] = sum([R1[s] if action_t[i] else R0[s] for i,s in enumerate(bandits)])
            bandits = np.array([transition(P1, s) if action_t[i] else transition(P0, s) for i, s in enumerate(bandits)])

            t += 1

    return values, policy_1, bandits, bound

def confidence_intervals(mat):
    """
    mat is a (number of experiments, number of differents argument for the exeperiment) shaped matrix
    Computes the confidence intervals at level 95% associated with to the matrix
    """
    repeat, size = mat.shape

    mean_values = np.array([np.mean(mat[:, i]) for i in range(size)])
    ci_values = np.array([1.96 * np.std(mat[:, i])/np.sqrt(repeat-1) for i in range(size)])

    return mean_values, ci_values