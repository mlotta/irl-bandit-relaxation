import numpy as np
import matplotlib.pyplot as plt
from relax_bandits import experiment, PR_construct, confidence_intervals
from qft import find_fixed_point_by_direct_calculation, test_unique_fixed_point, upper_reward, init_condition, one_simulation
from simulation_whittle import simulate_with_confidence

from timeit import default_timer as timer

from relax_bandits import memory 
experiment_joblib = memory.cache(experiment)
one_simulation_joblib = memory.cache(one_simulation)
test_unique_fixed_point_joblib = memory.cache(test_unique_fixed_point)


class Problem:
    """
    Simulation environnement for bandit problems, with know transition matrices
    and reward functions
    Implements simulation of the relaxation and whittle policies along with
    plotting functions
    nb_state: number of states
    nb_bandits: number of bandits (N)
    alpha: proportion of bandit to activate
    time_horizon: time horizon (T_H) on which to solve the linear program
    PR = [P0, P1, R0, R1]: transition matrices and reward vectors (default to random)
    tau: number of timesteps before refreshing the computation of the linear program 
    for relaxation policy (default to 1)
    time_exp: total horizon of simulation (T) (default to time_horizon)
    seed: for the random construction of PR (default to None)
    repeat_times: number of times to repeat each simulation (default to 50)
    number_bandit_list: vector of number of bandits with which simulations are made
    time_horizon_list: vector of time horizons with which simulations are made
    tau_list: vector of tau values with which simulations are made
    """
    def __init__(self, nb_state, nb_bandits, alpha, time_horizon, 
                 PR=None, tau=1, time_exp=None, seed=None, repeat_times=50,
                 number_bandits_list = range(10, 110, 10), time_horizon_list = [1, 5, 10, 20, 50, 100],
                 tau_list = [1, 5, 10, 20, 50, 100]):
        
        self.action_space = np.array([0, 1])
        self.nb_state = nb_state
        self.alpha = alpha
        self.nb_bandits = nb_bandits
        self.time_update(time_horizon, tau, time_exp)
        
        if PR is None:
            self.P0, self.P1, self.R0, self.R1 = PR_construct(self.nb_state, seed=seed)
            self.R0 = [0 for _ in range(self.nb_state)]
        else:
            self.P0, self.P1, self.R0, self.R1 = PR
            
        self.repeat_times = repeat_times  
        
        self.number_bandits_list = list(number_bandits_list)
        self.time_horizon_list = list(time_horizon_list)
        self.tau_list = list(tau_list) 
        self.seed = 1
        
 
    def time_update(self, time_horizon, tau=1, time_exp=None):
        self.time_horizon = time_horizon
        self.tau = tau
        if time_exp is None:
            self.time_exp = time_horizon
        else:
            self.time_exp = time_exp
            
    def experiment(self, debug=False, policy=False, joblib_id=0):

        values = np.zeros(self.time_exp)
        bandits = np.zeros(self.nb_bandits, dtype = int)
        #bandits = np.array([np.random.randint(0, self.nb_state) for _ in range(self.nb_bandits)])
        bounds = []
        t = 0

        if 0 == joblib_id:
            while t < self.time_exp:

                values[t:t+self.time_horizon], policy_1, bandits, temp_bounds = experiment_joblib(self.nb_state, self.alpha, self.nb_bandits, 
                                                                                  min(self.time_exp-t, self.time_horizon), 
                                                                                  [self.P0, self.P1], [self.R0, self.R1],
                                                                                  debug, step_resolution=self.tau,
                                                                                     bandits=bandits, seed=self.seed)

                self.seed += 1
                bounds.append(temp_bounds)
                t += self.time_horizon
 
        else:
            while t < self.time_exp:
                values[t:t+self.time_horizon], policy_1, bandits, temp_bounds = experiment(self.nb_state, self.alpha, self.nb_bandits, 
                                                                                  min(self.time_exp-t, self.time_horizon), 
                                                                                  [self.P0, self.P1], [self.R0, self.R1],
                                                                                  debug, step_resolution=self.tau,
                                                                                     bandits=bandits, seed=self.seed, joblib_id=joblib_id)

                self.seed += 1
                bounds.append(temp_bounds)
                t += self.time_horizon

        if not policy : return values, np.mean(bounds) 
        else: return values, np.mean(bounds), policy_1
    
    def simulation_time_variable(self):
        init_value_tau, init_value_time_horizon, init_value_time_exp = self.tau, self.time_horizon, self.time_exp
        total_values = np.zeros((self.repeat_times, len(self.time_horizon_list)))
        bounds = np.zeros(len(self.time_horizon_list))
        
        for i, t in enumerate(self.time_horizon_list):
            self.time_horizon = t
            for _ in range(self.repeat_times):
                values, bound = self.experiment()
                total_values[_, i] = sum(values)/(self.nb_bandits*self.time_exp)
                bounds[i] += bound
            bounds[i] /= self.repeat_times*self.time_horizon
        
        self.time_update(init_value_time_horizon, init_value_tau, init_value_time_exp)
        mean_values, ci_values = confidence_intervals(total_values)
        return (mean_values, ci_values), bounds

    def simulation_n_variable(self):
        init_value = self.nb_bandits
        total_values = np.zeros((self.repeat_times, len(self.number_bandits_list)))
        bounds = np.zeros(len(self.number_bandits_list))

        for i, N in enumerate(self.number_bandits_list):
            self.nb_bandits = N
            for _ in range(self.repeat_times):
                values, bound = self.experiment()
                total_values[_, i] = sum(values)/(self.nb_bandits*self.time_exp)
                bounds[i] += bound

        self.nb_bandits = init_value
        mean_values, ci_values = confidence_intervals(total_values)

        return (mean_values, ci_values), np.mean(bounds)/(self.repeat_times*self.time_horizon)
        
    def simulation_tau_variable(self):
        init_value_tau, init_value_time_horizon, init_value_time_exp = self.tau, self.time_horizon, self.time_exp
        total_values = np.zeros((self.repeat_times, len(self.tau_list)))
        bounds = np.zeros(len(self.tau_list))

        
        for i, tau in enumerate(self.tau_list):
            self.time_update(self.time_horizon, tau)
            for _ in range(self.repeat_times):
                values, bound = self.experiment()
                total_values[_, i] = sum(values)/(self.nb_bandits*self.time_exp)
                bounds[i] += bound

        
        self.time_update(init_value_time_horizon, init_value_tau, init_value_time_exp)
        mean_values, ci_values = confidence_intervals(total_values)

        return (mean_values, ci_values), np.mean(bounds)/(self.repeat_times*self.time_horizon)
        
        
    def plot_sim_nbbandits(self, name = None, whittle=None, new_fig=True):
        
        total_values, r_ubound = self.simulation_n_variable()
        l = np.linspace(self.number_bandits_list[0], self.number_bandits_list[-1], 100)

        if new_fig:
            plt.figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
            plt.rc('legend',fontsize=14)

        if whittle is not None:
            w = self.whittle_simulation()

            plt.plot(list(self.number_bandits_list), w[0], 'b',label = 'Whittle index policy')
            plt.fill_between(list(self.number_bandits_list), w[0]-w[1], w[0]+w[1], alpha=0.1, color='b')
            #if new_fig: plt.legend(loc='lower right')

        plt.plot(list(self.number_bandits_list), total_values[0], 'm',label = 'LP relaxation policy - T = '+str(self.time_exp))
        plt.fill_between(list(self.number_bandits_list), total_values[0]-total_values[1], total_values[0]+total_values[1], alpha=0.1, color='m')
        plt.plot(l,[r_ubound]*100,'k--',label = 'Theoretical upper-bound - finite time')
        if new_fig: plt.legend(loc='lower right')
        if new_fig: 
            plt.legend(loc='lower right', bbox_to_anchor=(1, -.3))
            plt.xlabel('Number of bandits (N)',fontsize = 25)
            plt.ylabel('Reward per bandit per unit time',fontsize = 25)

        if name is not None:
            plt.savefig(name)

            
    def plot_sim_timehorizon(self, name = None):
        
        total_values, bounds = self.simulation_time_variable()
        #V_upper = self.whittle_simulation(infinite_bound=True)

        plt.figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.rc('legend',fontsize=14)
        l = np.linspace(self.time_horizon_list[0], self.time_horizon_list[-1], 100)

        plt.plot(list(self.time_horizon_list), total_values[0], 'm',label = 'LP relaxation policy - N = '+str(self.nb_bandits))
        plt.fill_between(list(self.time_horizon_list), total_values[0]-total_values[1], total_values[0]+total_values[1], alpha=0.1, color='m')
        #plt.plot(list(self.time_horizon_list), total_values, 'm',label = 'LP relaxation policy - N = '+str(self.nb_bandits))
        plt.legend(loc='lower right')

        #plt.plot(list(self.time_horizon_list), bounds, 'k--',label = 'Theoretical upper-bound - finite time')
        plt.plot(l,[bounds[-1]]*100,'k--',label = 'Theoretical upper-bound - finite time')
        plt.legend(loc='lower right')
        
        plt.xlabel('Time horizon (T_H)',fontsize = 25)
        plt.ylabel('Reward per bandit per unit time',fontsize = 25)
        

        if name is not None:
            plt.savefig(name)
          
    def plot_sim_tau(self, name = None):
        
        total_values, r_ubound = self.simulation_tau_variable()
        l = np.linspace(self.tau_list[0], self.tau_list[-1], 100)

        plt.figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.rc('legend',fontsize=14)

        #plt.plot(list(self.tau_list), total_values, 'm',label = 'LP relaxation policy - N = '+str(self.nb_bandits) +' - T = '+str(self.time_horizon))
        plt.plot(list(self.tau_list), total_values[0], 'm', label = 'LP relaxation policy - N = '+str(self.nb_bandits) +' - T = '+str(self.time_horizon))
        plt.fill_between(list(self.tau_list), total_values[0]-total_values[1], total_values[0]+total_values[1], alpha=0.1, color='m')
        
        plt.plot(l,[r_ubound]*100,'k--',label = 'Theoretical upper-bound - finite time')
        plt.legend(loc='lower right', bbox_to_anchor=(1, -.3))

        
        plt.xlabel('Policy update timestep (tau)',fontsize = 25)
        plt.ylabel('Reward per bandit per unit time',fontsize = 25)
        #plt.title('Performance of the LP relaxation policy versus tau', fontsize=25)

        
        if name is not None:
            plt.savefig(name)  
            
    def plot_policy_evolution(self, name = None):
        prev_tau = self.tau
        self.tau = 1
        value, bounds, policy = self.experiment(policy=True)
        self.tau = prev_tau
        
        plt.figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.rc('legend',fontsize=14)
        for s in range(self.nb_state):
            plt.plot(list(range(1, self.time_horizon+1)), policy[:, s], label="state "+str(s+1))
        plt.legend(loc='lower right', bbox_to_anchor=(1, -.3))
        
        plt.xlabel('Time',fontsize = 25)
        plt.ylabel('Value of states',fontsize = 25)
        #plt.title('Evolution of the LP relaxation policy with tau=1', fontsize=25)
        
        
        if name is not None:
            plt.savefig(name)  
        
    def time_measure(self, repeat):  
        values = []
        for _ in range(repeat):
            t1 = timer()
            self.experiment(joblib_id=1)

            t2 = timer()
            values.append(t2-t1)
            
        return np.mean(values)
    
    def execution_time_th(self, repeat=10, plot=True):
        init_time = self.time_horizon
        init_P0, init_P1, init_R0, init_R1 = self.P0, self.P1, self.R0, self.R1

        iterated_on = range(1, 150, 10)
        measures = []
        for t in iterated_on:
            self.time_update(t)
            values = []
            for _ in range(repeat):
                self.P0, self.P1, self.R0, self.R1 = PR_construct(self.nb_state)
                values.append(self.time_measure(1))
            measures.append(np.mean(values))
            
        self.time_horizon = init_time
        self.P0, self.P1, self.R0, self.R1 = init_P0, init_P1, init_R0, init_R1

        xlab = "Time horizon"

        if plot: self.plot_execution_time(xlab, iterated_on, measures, 0, repeat)
            
    def execution_time_n(self, repeat=10, plot=True):
        init_nb = self.nb_bandits
        init_P0, init_P1, init_R0, init_R1 = self.P0, self.P1, self.R0, self.R1
        
        iterated_on = range(1, 150, 5)
        measures = []
        for n in iterated_on:
            self.nb_bandits = n
            values = []
            for _ in range(repeat):
                self.P0, self.P1, self.R0, self.R1 = PR_construct(self.nb_state)
                values.append(self.time_measure(1))
            measures.append(np.mean(values))
            
        self.nb_bandits = init_nb
        self.P0, self.P1, self.R0, self.R1 = init_P0, init_P1, init_R0, init_R1

        xlab = "Number of bandits"

        if plot: self.plot_execution_time(xlab, iterated_on, measures, 1, repeat)
           
    def execution_time_tau(self, repeat=10, plot=True):
        init_nb = self.nb_state
        init_P0, init_P1, init_R0, init_R1 = self.P0, self.P1, self.R0, self.R1
        iterated_on = range(2, 40, 5)
        measures = []
        for n in iterated_on:
            self.nb_state = n
            values = []
            for _ in range(repeat):
                self.P0, self.P1, self.R0, self.R1 = PR_construct(self.nb_state)
                values.append(self.time_measure(1))
            measures.append(np.mean(values))
        
        self.nb_state = init_nb
        self.P0, self.P1, self.R0, self.R1 = init_P0, init_P1, init_R0, init_R1
        
        xlab = "Number of states"

        if plot: self.plot_execution_time(xlab, iterated_on, measures, 2, repeat)

    def plot_execution_time(self, xlab, iterated_on, measures, mode, repeat):
        plt.figure(num=None, figsize=(15, 9), dpi=80, facecolor='w', edgecolor='k')
        plt.rc('legend',fontsize=18)

        plt.xlabel(xlab,fontsize = 25)
        plt.ylabel('Execution time',fontsize = 25)
        plt.ylim(bottom=0, top=np.max(measures)*1.2)

        plt.plot(iterated_on, measures, 'b')
        plt.savefig("Time_measure_m"+str(mode)+'_r'+str(repeat))
        
        #return iterated_on, measures
    
    def whittle_simulation(self, infinite_bound = False):

        if infinite_bound: 
            # Upper bound calculation 
            m = find_fixed_point_by_direct_calculation(self.P0,self.P1,self.alpha)
            V_upper = upper_reward(m, self.alpha, self.R1)        


            return V_upper
        else:
            w = np.array([simulate_with_confidence(N, int(round(N*self.alpha)), self.P0, self.P1, self.R1, repeat_times = self.repeat_times, time_horizon=self.time_exp) for N in self.number_bandits_list])
            mean_w, ci_w = confidence_intervals(w.transpose())
            return mean_w, ci_w