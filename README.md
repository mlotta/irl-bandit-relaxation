# Performance d'une relaxation linéaire pour le bandit markovien

Ce répertoire contient le code produit lors d'un projet d'Introduction à la Recherche
en Laboratoire de deuxième année Ensimag. Le projet concernait l'étude de la 
performance d'une nouvelle politique pour le bandit markovien restless.

Le sujet : 
https://ensiwiki.ensimag.fr/images/0/05/Project_bandits.pdf

Un wiki synthétique:
https://ensiwiki.ensimag.fr/index.php?title=Matthias_Lotta_:_Performance_of_index_policies_for_markovian_bandits

Le rapport:
https://ensiwiki.ensimag.fr/images/1/16/Rapport_mlotta.pdf

## Organisation du répertoire

Le répertoire s'organise en 5 fichiers. Le code est réalisé en python.

* `relaxation_demo.ipynb` est un notebook de démonstration. Il contient les principales fonctions utiles et permet de reproduire des simulations du rapport.
* `relax_bandit.py` contient la définition de la politique de relaxation.
* `Problem.py` est un environnement de simulation et contient des outils graphiques.
* `qft.py` calcule la politique des index de Whittle. Travail réalisé par Chen YAN.
* `simulation_whittle.py` est un environnement de simulation pour les index de Whittle. Travail réalisé par Chen YAN.

Nous utilisons la librarie `joblib` pour enregistrer les résultats des simulations. Dans un premier temps il faut donc les refaire. Le cache, initialement vide, se trouve dans le dossier `cachedir`. 

Auteur : Matthias LOTTA

Tuteur : Nicolas GAST

Crédit index de Whittle : Chen YAN